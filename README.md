# Python Flask for Beginners #

Installing flask environment

```python
python -m venv venv

venv\Scripts\activate.bat


pip list

pip install Flask
```

https://purecss.io/


## Challenge - BMI Calculator Project ##

Main objectives:
- Allow a user to enter thier weight in kilograms and their height in cm.
- Once the data is submitted, the user is presented with their calculate BMI.


## PyBites

https://www.pybit.es


## Screenshot BMI claculator ##

![Screenshot](bmi-calc.png)
