#!python3

from flask import Flask, request, render_template

app = Flask(__name__)

@app.route('/')
def rootpage():
    return render_template("index.html")

@app.route('/form', methods=['GET','POST'])
def formpage():
    name = ''
    food = ''
    if request.method == 'POST' and 'username' in request.form:
        name = request.form.get('username')
        food = request.form.get('favoritefood')
    return render_template("form.html",name = name, food = food)

app.run()