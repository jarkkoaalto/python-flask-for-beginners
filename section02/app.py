#!python3

from flask import Flask, request

app = Flask(__name__)

@app.route('/')
def welcome():
    return 'This is Flask application!'

@app.route('/one')
def welcomeome():
    return 'This in one page application'

@app.route('/two')  
def welcometwo():
    return 'This is two page application!'

@app.route('/method', methods=['GET','POST'])
def method():
    if request.method == 'POST':
        return "You're used a POST request!"
    else:
        return "I reckon you're probably using a GET request!"

app.run()